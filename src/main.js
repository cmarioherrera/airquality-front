import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'leaflet/dist/leaflet.css'
import './assets/css/open-weather-icons/css/open-weather-icons.css';
import './assets/css/weather-icons/css/weather-icons.min.css';
import './assets/css/weather-icons/css/weather-icons-wind.min.css';


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
